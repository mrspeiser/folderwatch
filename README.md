Folderwatch is a tiny program built around the open source iNotify-tools that incorporates named pipes to trigger events once a number of specific files are created and matched inside a directory.

Where do I clone this repo?
- We recommend cloning this repo into either the /opt or /etc folder

What do I do after I clone?
- Navigate into the main directory and run the 'init' file. This will initialize the program and download the inotify tools

The program should start up automatically once you run the 'init' file.

Follow the menus and provide FULL PATHS to any prompt that asks you to define a file. That means start from the root directory /home/yourusername, /var/something/, sftp/foldername

GOTCHAS:
- When providing the script that runs after the check returns true, make sure that inside the script you provide FULL PATHS to the command. 